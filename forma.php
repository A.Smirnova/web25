<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="style.css">
        <title>web 2.3</title>
    </head>
<body>
<?php
if (!empty($messages)) {
    print $messages['can_login'];
    if(!empty($_SESSION['login'])){
        printf($messages['login'], $_SESSION['login'], $_SESSION['uid']);
        echo '<a href="login.php?exit=1">Выход</a>';}
    else echo '<a href="login.php">Вход</a>';
}
?>
  <h1 class="center">Форма для заполнения</h1>
  <form name="forma" class="box" action="" method="POST">
  
  <?php if (!empty($messages)){
                    echo '
         <div class="forminfo">
                    ';
                    print $messages['res_saved'];
                    echo '
         </div>
                    ';
                }
                    ?>


      <label><b>Имя:</b><br />
        <input name="name" placeholder="Имя" type="text" <?php print 'value="'.$values['name'].'"'?>/>
		<?php if($errors['name']) {print $messages['name'];} ?>
      </label><br />

      <label><b>Почта email:</b><br />
        <input name="email" placeholder="Email" type="text" <?php print 'value="'.$values['email'].'"'?>/>
		<?php if($errors['email']) print $messages['email']; ?>

      </label><br />

      <label>
        <b>Год рождения: </b>
        <br /><br />
        <select name="year" >
              <option value="1985" <?php if ($values['year']=='1985')print 'selected'?>>1985</option>
              <option value="1986" <?php if ($values['year']=='1986')print 'selected'?>>1986</option>
              <option value="1987" <?php if ($values['year']=='1987')print 'selected'?>>1987</option>
              <option value="1988" <?php if ($values['year']=='1988')print 'selected'?>>1988</option>
              <option value="1989" <?php if ($values['year']=='1989')print 'selected'?>>1989</option>
              <option value="1990" <?php if ($values['year']=='1990')print 'selected'?>>1990</option>
              <option value="1991" <?php if ($values['year']=='1991')print 'selected'?>>1991</option>
              <option value="1992" <?php if ($values['year']=='1992')print 'selected'?>>1992</option>
              <option value="1993" <?php if ($values['year']=='1993')print 'selected'?>>1993</option>
              <option value="1994" <?php if ($values['year']=='1994')print 'selected'?>>1994</option>
              <option value="1995" <?php if ($values['year']=='1995')print 'selected'?>>1995</option>
              <option value="1996" <?php if ($values['year']=='1996')print 'selected'?>>1996</option>
              <option value="1997" <?php if ($values['year']=='1997')print 'selected'?>>1997</option>
              <option value="1998" <?php if ($values['year']=='1998')print 'selected'?>>1998</option>
              <option value="1999" <?php if ($values['year']=='1999')print 'selected'?>>1999</option>
              <option value="2000" <?php if ($values['year']=='2000')print 'selected'?>>2000</option>
              <option value="2001" <?php if ($values['year']=='2001')print 'selected'?>>2001</option>
              <option value="2002" <?php if ($values['year']=='2002')print 'selected'?>>2002</option>
              <option value="2003" <?php if ($values['year']=='2003')print 'selected'?>>2003</option>
              <option value="2004" <?php if ($values['year']=='2004')print 'selected'?>>2004</option>
              <option value="2005" <?php if ($values['year']=='2005')print 'selected'?>>2005</option>
        </select>
		<?php if($errors['year']) print $messages['year']; ?>
      </label><br /><br />

      
      <b>Пол:</b><br />
	  
	  <?php $gender = [
                1 => ['str' => 'М','attr' => ''],
                2 => ['str' => 'Ж','attr' => '']
        ];
        if(!empty($values['gender'])){
            $gender[$values['gender']]['attr'] = 'checked';
        }
        foreach($gender as $key => $val) {
            print '<label>
                    <input type="radio" name="gender" value="' . $key . '" ' . $val['attr'] . '>' . $val['str'] . '
                   </label>';
        }
        if($errors['gender']) print $messages['gender'];
        ?><br /><br />

      <b>Количество конечностей:</b><br />
      <label><?php $limbs = [
            '0' => '',
            '1' => '',
            '2' => '',
            '3' => '',
            '4' => ''
        ];
        if (!empty($values['limbs'])){
            $limbs[$values['limbs']] = 'checked';
        }
        foreach($limbs as $key => $val){
            print '<label>
        <input type="radio" name="limbs" value="'.$key.'" '.$val.'>'.$key.'
      </label>';
        }
        if($errors['limbs']) print $messages['limbs'];
        ?>
</label><br /><br />

        <label>
        <b>Сверхспособности:</b>
        <br /><br />
		<select name="superpowers[]" multiple="multiple">
		<?php
          $user = 'u20622';
          $pass = '2079517';
          try {
              $db = new PDO('mysql:host=localhost;dbname=u20622', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
              $sql = "select * from superpowers";
              $stmt = $db->prepare($sql);
              $stmt->execute();
              foreach($stmt as $row) {
                  if (in_array($row['id_power'], $values['superpowers'])) {
                      $mes = 'selected';
                  } else $mes = '';
                  print '<option value="' . $row['id_power'] . '" ' . $mes . '>' . $row['name_power'] . '</option>';
              }
          }
          catch (PDOException $e) {
          print('Error : ' . $e->getMessage());
          exit();
          }?>
		  </select>
		<?php if($errors['superpowers']) print $messages['superpowers']; ?>
      </label><br /><br />

      <label>
        <b>Биография:</b><br /><br />
        <textarea name="biography"><?php print $values['biography']?></textarea>
		<?php if($errors['biography']) print $messages['biography']; ?>

      </label><br />
      <br />

      С контрактом 
      <label><input type="checkbox" name="check" <?php if ($values['check']=='on')print 'checked'?>/> ознакомлен</label><?php if($errors['check']) print $messages['check']; ?>
	  <br />
      <br />
      <br />
      <br />

      Проверьте ещё раз все пункты. Если информация заполнена верно, то нажмите "Отправить":
      <input type="submit" value="Отправить" />
    </form>
    <footer>
        <div class="center">© Анастасия Смирнова 2021</div><br /><br />
    </footer>
</html>
