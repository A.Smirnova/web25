<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.
session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login'])) {
  // Если есть логин в сессии, то пользователь уже авторизован.
  // TODO: Сделать выход (окончание сессии вызовом session_destroy()
  //при нажатии на кнопку Выход).
  // Делаем перенаправление на форму.
  if ($_SERVER['REQUEST_METHOD'] == 'GET' and $_GET['exit'])
        session_destroy();
    else header('Location: ./');
}
$message;
// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
?>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>web25</title>
  <link rel="stylesheet" href="style.css">
  </head>
  <div>
<form class="box" action="" method="post" >
  <label>
    Логин
  <input name="login" <?php if (!empty($_COOKIE['login'])) print 'value="'.$_COOKIE['login'].'"'?>/>
  </label>
  <label>
    Пароль
  <input name="pass" <?php if (!empty($_COOKIE['pass'])) print 'value="'.$_COOKIE['pass'].'"'?>/>
  </label>
<?php
if (!empty($_GET['none'])) {
  $txt="Неправильный логин или пароль.";
    print'<div class="error">' . $txt . '<div>';
}
?>
  <br /><br />
  <input type="submit" value="Войти" />
    <div style="padding: 20px 0 10px 0">
    <a href="index.php">Регистрация</a>
    </div>
</form>
</div>


<?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {

  // TODO: Проверть есть ли такой логин и пароль в базе данных.
  // Выдать сообщение об ошибках.
  
  $user = 'u20622';
  $pass = '2079517';
  $db = new PDO('mysql:host=localhost;dbname=u20622', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  $login = $_POST['login'];
  $pass = $_POST['pass'];
  
  $stmt = $db->prepare("SELECT id, passhash FROM users WHERE login = :login");
  $stmt->execute(['login' => $login]);
  $user_id = $stmt ->fetch(PDO::FETCH_ASSOC);
  if($user_id['id'] and password_verify($_POST['pass'], $user_id['passhash'])){
  // Если все ок, то авторизуем пользователя.
  $_SESSION['login'] = $_POST['login'];
  // Записываем ID пользователя.
  $_SESSION['uid'] = $user_id['id'];
  $_COOKIE[session_name()] = "session_true";
  // Делаем перенаправление.
  header('Location: ./');
  }
  else {
	  header('Location: ?none=1');
  }
}
